/**
 * Created by AndreyMaznyak on 14.04.2016.
 */
'use strict';

var fs = require('fs');

if(module.parent == null){
    readMetaDataFile(function(err, result){
        parserMetaData(result.toString());
    });
}

function readMetaDataFile(callback){
    fs.readFile('1Cv7.DDS','UTF-8', function(err, result){
        if(err)
            console.log(err);
        else{
            //console.log(result.toString());
        }
        if(callback){
            callback(err, result);
        }
    });
}

function parserMetaData(metadatastr){
    var regex_table = new RegExp(/*'(?:#===============================================================================\n#==)'+
                                 */'(TABLE)(.+\n){3,}?'+
                                 '(?=#===============================================================================\n#==(TABLE|PROCEDURE))','gm'),
        regex_table_title = new RegExp('(?:TABLE no )(?:.+: )(.+?)(?:\\n)'),
        regex_table_name = new RegExp('(?:RecordLock\\nT=)(.+?)(?: |\\|)'),
        regex_filds = new RegExp('(?:F=)((?:.){1,}?)(?: )(?:\\|)((?:.){1,}?)(?:\\|)(.)(?:.{1,}?\\|)([0-9]+)(?:.{1,}?\\|)([0-9]+)', 'g'),
    results = metadatastr.match(regex_table),
    results_obj = [];
    for(var i = 0; i < results.length; i++){
        //console.log(results[i]);
        var metaDataObj = {};
        metaDataObj.title = results[i].match(regex_table_title)[1];
        metaDataObj.name = results[i].match(regex_table_name)[1];
        metaDataObj.fields = [];
        var matches_fiels = results[i].match(regex_filds);
        for(var k = 0; k < matches_fiels.length; k++){
            var fieldsStr = regex_filds.exec(matches_fiels[k]);
            var field_obj = {};
            if(!!fieldsStr&&fieldsStr.length > 0){
                field_obj = {
                    name:fieldsStr[1].trim(),
                    title:fieldsStr[2].trim(),
                    type:fieldsStr[3],
                    length:fieldsStr[4],
                    precision:fieldsStr[5]
                };
                metaDataObj.fields.push(field_obj);
                //console.log(field_obj);
            }
        }
        console.log(metaDataObj);
        results_obj.push(metaDataObj);
    }

    console.log(results.length);

    fs.writeFile('schema1c.JSON', JSON.stringify(results_obj, null, "\t"), function(err){
        if (err) throw err;
        console.log('It\'s saved!');
    });
}

module.expoets ={
    readMetaDataFile: readMetaDataFile
}

